import matplotlib.pyplot as plt
import mplleaflet
import pandas as pd

def leaflet_plot_stations(binsize, hashid):

    df = pd.read_csv('data/C2A2_data/BinSize_d{}.csv'.format(binsize))

    station_locations_by_hash = df[df['hash'] == hashid]

    lons = station_locations_by_hash['LONGITUDE'].tolist()
    lats = station_locations_by_hash['LATITUDE'].tolist()

    plt.figure(figsize=(8,8))

    plt.scatter(lons, lats, c='r', alpha=0.7, s=200)

    return mplleaflet.display()

leaflet_plot_stations(400,'57190486e180f0888df6495c6149269b83c0f07bdae37d1eea199f6e')

import numpy as np

df = pd.read_csv('data/C2A2_data/BinnedCsvs_d400/57190486e180f0888df6495c6149269b83c0f07bdae37d1eea199f6e.csv')
df['Date']=pd.to_datetime(df['Date'])
df.set_index('Date',inplace=True)
df.sort_index(inplace=True)
df = df[~((df.index.month == 2) & (df.index.day == 29))]

###2005-2014 data
df_2014=df[df.index.year<2015]
df_2014.index=df_2014.index.strftime('%d%b')
df2014=df_2014.groupby(df_2014.index,sort=False)['Data_Value'].agg(['min','max','mean'])

###2015 data
df_2015=df[df.index.year>2014]
df_2015.index=df_2015.index.strftime('%d%b')
df2015=df_2015.groupby(df_2015.index,sort=False)['Data_Value'].agg(['min','max'])


###scatter data
df_scatter_min=df2015['min'].where(df2015['min']<df2014['min'])
df_scatter_max=df2015['max'].where(df2015['max']>df2014['max'])

###chart
plt.figure(figsize=(20,10))
mintemp=list(df2014['min']/10)
maxtemp=list(df2014['max']/10)
meantemp=list(df2014['mean']/10)
plt.plot(maxtemp,'-',c='#D98880',linewidth=2.5, label='_nolegend_')
plt.plot(mintemp,'-',c='#7FB3D5',linewidth=2.5, label='_nolegend_')
#plt.plot(meantemp,'-',c='grey',linewidth=2.0,alpha=0.15)
plt.legend()
plt.gca().fill_between(range(len(df2014)), 
                       mintemp, maxtemp, 
                       facecolor='#999999', alpha=.1, label='_nolegend_')
plt.scatter(np.arange(1,366),df_scatter_max/10,color='#922B21',s=80)
plt.scatter(np.arange(0,365),df_scatter_min/10,color='#1F618D',s=80)

###dejunkifying
import calendar
plt.title('2015 temperature records compared with \n max and min daily temperature for the period 2004-2014 in Madrid Area', alpha=0.8,fontsize=25)
plt.xlabel('Months',fontsize=25)
plt.ylabel('Temperature (Celsius)',fontsize=25)
plt.tick_params(top='off', bottom='off', left='off', right='off', labelleft='on', labelbottom='on')
for spine in plt.gca().spines.values():
    spine.set_visible(False)

plt.xticks([15.5,45,74.5,105,135.5,166,196.5,227.5,258,288.5,319,349.5], calendar.month_abbr[1:13], alpha=0.8, fontsize=21)
plt.yticks(np.arange(-10,41,step=5),np.arange(-10,41,step=5),alpha=0.8,fontsize=21)
plt.grid(color='grey', linestyle='-', linewidth=.5, alpha=.5)
plt.annotate('Max 2004-2014', xy=(20,32), xytext=(40,32), fontsize=24, color='#D98880')
plt.annotate('Min 2004-2014', xy=(20,32), xytext=(172,2), fontsize=24, color='#7FB3D5')
plt.legend(fontsize=21, frameon=False, labels=['2015 Max records',
                                              '2015 Min records', 'Max', 'Min'])

plt.show()