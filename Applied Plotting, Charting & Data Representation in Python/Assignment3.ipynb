{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Assignment 3 - Building a Custom Visualization\n",
    "\n",
    "---\n",
    "\n",
    "In this assignment you must choose one of the options presented below and submit a visual as well as your source code for peer grading. The details of how you solve the assignment are up to you, although your assignment must use matplotlib so that your peers can evaluate your work. The options differ in challenge level, but there are no grades associated with the challenge level you chose. However, your peers will be asked to ensure you at least met a minimum quality for a given technique in order to pass. Implement the technique fully (or exceed it!) and you should be able to earn full grades for the assignment.\n",
    "\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ferreira, N., Fisher, D., & Konig, A. C. (2014, April). [Sample-oriented task-driven visualizations: allowing users to make better, more confident decisions.](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/Ferreira_Fisher_Sample_Oriented_Tasks.pdf) \n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In Proceedings of the SIGCHI Conference on Human Factors in Computing Systems (pp. 571-580). ACM. ([video](https://www.youtube.com/watch?v=BI7GAs-va-Q))\n",
    "\n",
    "\n",
    "In this [paper](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/Ferreira_Fisher_Sample_Oriented_Tasks.pdf) the authors describe the challenges users face when trying to make judgements about probabilistic data generated through samples. As an example, they look at a bar chart of four years of data (replicated below in Figure 1). Each year has a y-axis value, which is derived from a sample of a larger dataset. For instance, the first value might be the number votes in a given district or riding for 1992, with the average being around 33,000. On top of this is plotted the confidence interval -- the range of the number of votes which encapsulates 95% of the data (see the boxplot lectures for more information, and the yerr parameter of barcharts).\n",
    "\n",
    "<br>\n",
    "<img src=\"readonly/Assignment3Fig1.png\" alt=\"Figure 1\" style=\"width: 400px;\"/>\n",
    "<h4 style=\"text-align: center;\" markdown=\"1\">  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Figure 1 from (Ferreira et al, 2014).</h4>\n",
    "\n",
    "<br>\n",
    "\n",
    "A challenge that users face is that, for a given y-axis value (e.g. 42,000), it is difficult to know which x-axis values are most likely to be representative, because the confidence levels overlap and their distributions are different (the lengths of the confidence interval bars are unequal). One of the solutions the authors propose for this problem (Figure 2c) is to allow users to indicate the y-axis value of interest (e.g. 42,000) and then draw a horizontal line and color bars based on this value. So bars might be colored red if they are definitely above this value (given the confidence interval), blue if they are definitely below this value, or white if they contain this value.\n",
    "\n",
    "\n",
    "<br>\n",
    "<img src=\"readonly/Assignment3Fig2c.png\" alt=\"Figure 1\" style=\"width: 400px;\"/>\n",
    "<h4 style=\"text-align: center;\" markdown=\"1\">  Figure 2c from (Ferreira et al. 2014). Note that the colorbar legend at the bottom as well as the arrows are not required in the assignment descriptions below.</h4>\n",
    "\n",
    "<br>\n",
    "<br>\n",
    "\n",
    "**Easiest option:** Implement the bar coloring as described above - a color scale with only three colors, (e.g. blue, white, and red). Assume the user provides the y axis value of interest as a parameter or variable.\n",
    "\n",
    "\n",
    "**Harder option:** Implement the bar coloring as described in the paper, where the color of the bar is actually based on the amount of data covered (e.g. a gradient ranging from dark blue for the distribution being certainly below this y-axis, to white if the value is certainly contained, to dark red if the value is certainly not contained as the distribution is above the axis).\n",
    "\n",
    "**Even Harder option:** Add interactivity to the above, which allows the user to click on the y axis to set the value of interest. The bar colors should change with respect to what value the user has selected.\n",
    "\n",
    "**Hardest option:** Allow the user to interactively set a range of y values they are interested in, and recolor based on this (e.g. a y-axis band, see the paper for more details).\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>0</th>\n",
       "      <th>1</th>\n",
       "      <th>2</th>\n",
       "      <th>3</th>\n",
       "      <th>4</th>\n",
       "      <th>5</th>\n",
       "      <th>6</th>\n",
       "      <th>7</th>\n",
       "      <th>8</th>\n",
       "      <th>9</th>\n",
       "      <th>...</th>\n",
       "      <th>3640</th>\n",
       "      <th>3641</th>\n",
       "      <th>3642</th>\n",
       "      <th>3643</th>\n",
       "      <th>3644</th>\n",
       "      <th>3645</th>\n",
       "      <th>3646</th>\n",
       "      <th>3647</th>\n",
       "      <th>3648</th>\n",
       "      <th>3649</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>1992</th>\n",
       "      <td>2793.851077</td>\n",
       "      <td>105341.500709</td>\n",
       "      <td>-44415.807259</td>\n",
       "      <td>-49859.545652</td>\n",
       "      <td>328367.085875</td>\n",
       "      <td>242510.874946</td>\n",
       "      <td>47436.181512</td>\n",
       "      <td>75761.922925</td>\n",
       "      <td>148853.385142</td>\n",
       "      <td>220465.210458</td>\n",
       "      <td>...</td>\n",
       "      <td>138454.070217</td>\n",
       "      <td>122488.069943</td>\n",
       "      <td>162247.982356</td>\n",
       "      <td>-273907.868554</td>\n",
       "      <td>-138410.570396</td>\n",
       "      <td>27638.756441</td>\n",
       "      <td>-33120.047151</td>\n",
       "      <td>-40989.824866</td>\n",
       "      <td>94532.974507</td>\n",
       "      <td>6128.841097</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1993</th>\n",
       "      <td>-44406.485331</td>\n",
       "      <td>180815.466879</td>\n",
       "      <td>-108866.427539</td>\n",
       "      <td>-114625.083717</td>\n",
       "      <td>196807.232582</td>\n",
       "      <td>47161.295355</td>\n",
       "      <td>136522.083654</td>\n",
       "      <td>58826.904901</td>\n",
       "      <td>23329.019613</td>\n",
       "      <td>-96417.638483</td>\n",
       "      <td>...</td>\n",
       "      <td>-37809.868064</td>\n",
       "      <td>93228.910228</td>\n",
       "      <td>108183.379950</td>\n",
       "      <td>146728.060346</td>\n",
       "      <td>-10083.899508</td>\n",
       "      <td>-31300.144215</td>\n",
       "      <td>95017.857057</td>\n",
       "      <td>164071.514663</td>\n",
       "      <td>14409.944591</td>\n",
       "      <td>33298.608969</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1994</th>\n",
       "      <td>134288.798913</td>\n",
       "      <td>169097.538334</td>\n",
       "      <td>337957.368420</td>\n",
       "      <td>-76005.273164</td>\n",
       "      <td>90130.207911</td>\n",
       "      <td>8453.626320</td>\n",
       "      <td>-24562.317561</td>\n",
       "      <td>195665.400438</td>\n",
       "      <td>-53475.640770</td>\n",
       "      <td>44708.230667</td>\n",
       "      <td>...</td>\n",
       "      <td>145216.405451</td>\n",
       "      <td>67773.006363</td>\n",
       "      <td>95711.194465</td>\n",
       "      <td>174500.629277</td>\n",
       "      <td>-27821.888075</td>\n",
       "      <td>-57881.583140</td>\n",
       "      <td>26321.525617</td>\n",
       "      <td>-21424.067186</td>\n",
       "      <td>60164.652898</td>\n",
       "      <td>-74750.286614</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1995</th>\n",
       "      <td>-44485.202120</td>\n",
       "      <td>-156.410517</td>\n",
       "      <td>-13425.878636</td>\n",
       "      <td>53540.999558</td>\n",
       "      <td>130408.559874</td>\n",
       "      <td>20445.656224</td>\n",
       "      <td>60336.077232</td>\n",
       "      <td>60688.099156</td>\n",
       "      <td>-12748.496722</td>\n",
       "      <td>57150.175074</td>\n",
       "      <td>...</td>\n",
       "      <td>-636.804950</td>\n",
       "      <td>49707.896672</td>\n",
       "      <td>52687.564135</td>\n",
       "      <td>13529.920850</td>\n",
       "      <td>67016.324752</td>\n",
       "      <td>41664.942829</td>\n",
       "      <td>119870.946944</td>\n",
       "      <td>56946.289297</td>\n",
       "      <td>67927.466106</td>\n",
       "      <td>32839.707999</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>4 rows × 3650 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "               0              1              2              3     \\\n",
       "1992    2793.851077  105341.500709  -44415.807259  -49859.545652   \n",
       "1993  -44406.485331  180815.466879 -108866.427539 -114625.083717   \n",
       "1994  134288.798913  169097.538334  337957.368420  -76005.273164   \n",
       "1995  -44485.202120    -156.410517  -13425.878636   53540.999558   \n",
       "\n",
       "               4              5              6              7     \\\n",
       "1992  328367.085875  242510.874946   47436.181512   75761.922925   \n",
       "1993  196807.232582   47161.295355  136522.083654   58826.904901   \n",
       "1994   90130.207911    8453.626320  -24562.317561  195665.400438   \n",
       "1995  130408.559874   20445.656224   60336.077232   60688.099156   \n",
       "\n",
       "               8              9         ...                3640  \\\n",
       "1992  148853.385142  220465.210458      ...       138454.070217   \n",
       "1993   23329.019613  -96417.638483      ...       -37809.868064   \n",
       "1994  -53475.640770   44708.230667      ...       145216.405451   \n",
       "1995  -12748.496722   57150.175074      ...         -636.804950   \n",
       "\n",
       "               3641           3642           3643           3644  \\\n",
       "1992  122488.069943  162247.982356 -273907.868554 -138410.570396   \n",
       "1993   93228.910228  108183.379950  146728.060346  -10083.899508   \n",
       "1994   67773.006363   95711.194465  174500.629277  -27821.888075   \n",
       "1995   49707.896672   52687.564135   13529.920850   67016.324752   \n",
       "\n",
       "              3645           3646           3647          3648          3649  \n",
       "1992  27638.756441  -33120.047151  -40989.824866  94532.974507   6128.841097  \n",
       "1993 -31300.144215   95017.857057  164071.514663  14409.944591  33298.608969  \n",
       "1994 -57881.583140   26321.525617  -21424.067186  60164.652898 -74750.286614  \n",
       "1995  41664.942829  119870.946944   56946.289297  67927.466106  32839.707999  \n",
       "\n",
       "[4 rows x 3650 columns]"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use the following data for this assignment:\n",
    "\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "\n",
    "np.random.seed(12345)\n",
    "\n",
    "df = pd.DataFrame([np.random.normal(33500,150000,3650), \n",
    "                   np.random.normal(41000,90000,3650), \n",
    "                   np.random.normal(41000,120000,3650), \n",
    "                   np.random.normal(48000,55000,3650)], \n",
    "                  index=[1992,1993,1994,1995])\n",
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Process flow\n",
    "- build bar chars for each year by using average value\n",
    "- change bar color by specifying a value\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib import cm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1992    34484.080607\n",
       "1993    39975.673587\n",
       "1994    37565.689950\n",
       "1995    47798.504333\n",
       "dtype: float64"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "avgs = df.mean(axis=1)\n",
    "avgs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "value_of_interest = 35000"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 86,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "value_of_interest = avgs.loc[1994]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 85,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYcAAAD8CAYAAACcjGjIAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz\nAAALEgAACxIB0t1+/AAAHC9JREFUeJzt3XlYVdXi//H3YgYRRFGcxZwNS8UyK4esa1pa3tSfNM/z\n12bTbsPXrG732zzeykqr26PlzWtqVzPTVCxnUzMnlEGcU0BQwMOwf39wRE8LFOQwiJ/X85zHc9Ze\ne521t4fzYe+91sY4joOIiMiJfKq7AyIiUvMoHERExKJwEBERi8JBREQsCgcREbEoHERExKJwEBER\ni8JBREQsCgcREbH4VXcHKkBTu0VEyseUtaKOHERExKJwEBERi8JBREQsCgcREbEoHERExKJwEBER\ni8JBREQsCgcREbEoHERExKJwEBERy5l8+wwROQuNM2W+A0StNM6pmjsH6chBREQsCgcREbEoHERE\nxKJwEBERi8JBREQsCgcREbFoKKuI1Hp1mzZl1JYtBISG8vfQUFxHjhDdty+3LVxYYv1tc+fy5cCB\nANz2009E9+tXYr1PevVi57JlAASFh3PlG2/QcehQfAMCSImPZ86oUaRt3+6xTszIkVzy5JM0aN+e\n3EOHSJo/nx/HjiVrzx6vba83KBxEpNYb8OqruA4fJiA0tLhsz5o1fHLRRR71wlu2ZMTUqWybM6e4\n7L8PPEBgWJhHvcvGj6dxt27sXrmyuGz411/TKCaGOQ8/zNFDh+jzzDPcMn8+H3TpwtGsLAA6DBnC\n8K++YsV77/HD6NHUbdKE/i++yA3//S8TYmNxqmgOQ1koHESkVmvVuzdtBw4k/u9/Z8BrrxWXH83K\nYufy5R51W/buTWFBAb9PnVpc9semTR51fP39adqjBxu+/prCggIAml90EW2vvJLPL7+cpAULANi5\nfDmPJCURe889/PL66wB0ueEGdq9ezexRo473IzOT62fOpEGHDhzYvNm7G18BuuYgIrWW8fFh0Lvv\nsmj8eLIPHDhl/S7XX0/KokUnPcXTduBAguvXZ8OUKcVljbt2pcDlIvmE01RH9u9n77p1tLv66uIy\nH39/jh465NFebkZGUV9r2MxvhYOI1Fo97rsPv8BAVrz//inrNmjXjibdu/PbCV/6JYmJi+NQaiop\n8fHFZX5BQRQWFOAUFnrULXC5aNipU/HrXydOpGXv3px/880E1q1Lg3bt6P/iiyTOn28doVQ3hYOI\n1ErB9evT/4UXmPvYYxTm55+yfkxcHAUuF5umTSu1jn9wMB2uucbjtBNA2rZt+AcH0ygmprjMLyiI\nRjExBNevX1yWMHs23952G0MmTOCpzExGbd2K8fVl6rBhp7GFlUvhICK10uUvvcTOZctIOOHi8snE\nxMWx/YcfyElPL7VO+yFDCAgN9TilBLB97lzSExMZ8tFHNGjfntDGjRn84YcEhYd7HE1E9+vH4A8/\nZNnbb/NZv378e+RIguvXZ+T06RifmvV1rAvSIlLrNOzcmW533MGkPn0ICg8HwD8kBIDA8HAKCwrI\nz80trh913nk07NyZxS+9dNJ2Y+LiOJiQwO7Vqz3KC/Ly+CYujmFTpjBqyxYAUuLjWffFF7Tu37+4\n3pWvv86WmTP5cezY4rK9a9cyassWOl57LZumT6/YhnuRwkFEap0G7drhGxDAXe45CCd6fNcu1nzy\nCTPvvru4LCYujrzsbLbMmFFqm4FhYbQbNIifX3mlxOW7Vq7knbZtadC+PYX5+aQnJnLDrFnF8yAA\nIjt2tK5pHNy6lbzsbCLatCnvZlYqhYOI1Do7lizhsz9NXGs7cCCXjh3Ll4MGkZ6Y6LEsJi6OLbNm\n4TpypNQ2O/31r/gFBZ3ygvXBrVsBqN+2LedccQWThwwpXpaRkkKT7t096kd27Ih/SAgZycll2LKq\no3AQkVon++BBkhct8iirFx0NwI74eI8QaN6zJxGtWzP30UdP2mZMXBx7164tdS5Cn2ee4cDmzWQf\nOEBUly70efZZNnz1FYk//lhcZ9WHHzLwzTfJ2r2bbXPmUCcqir7PPUd6UhIJs2ef5tZWDoWDiJzV\nYuLiyM3IOOmF65AGDWh9+eX89OyzJ60z8K23CImMJDM1lV9ee42l7slvxyx/5x0KXC4uuP9+etx3\nH7kZGexYsoT5Tz1FXna217bJG0xNmq5dTmdsx0Xk9OnPhFboq6/MO69mjZ0SEZEaQeEgIiIWhYOI\niFgUDiIiYlE4iIiIReEgIiIWhYOIiFgUDiIiYlE4iIiIReEgIiIWhYOIiFgUDiIiYlE4iIiIReEg\nIiIW/T0HKbcF4SHV3YVq1f9Qzbrvvkhl0JGDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJR\nOEilaHjtUGJ/WEDvpFT67kuj56q1RD8xBuPvX1zHLzycju99SO/knfTZtZ/zv/mW4HPOsdqKvHoI\nF/68nH770+m1fiMtHhzlsbzepb3pfyi7xMf5/5lRXK/xDTeVWKfpHXdV3o4QOUNpnoNUCv/6DUhf\nvIiUd94i/1AGYbE9aD32aQKiotg6+jEAzp30BaGdziVhzGjyMzOJHj2GbjNms/ziCyjIygIgvOdF\ndPlyCnu+/IJtz/6NsNgLaPP8iziFhez84H0AstatZdXlfT3eP6hFC2I++5KD836w+rZm8EAKc3KK\nX+ckJ1fSXhA5cykcpFLsnvSpx+uM+MX41Q2j2V33sHX0Y4RdcCENLv8Lv15zFemLFgJwaNVKLl6/\nkaa33UHqu28DED3mKQ4tW8rmUQ8AkLZgPn7h4bQe8xS7PpmAk5dHQVYWmatWerxfvYsvwSkoYP/0\n/1h9y1qzmoIjRyphq0VqD4WDVJm8tIP4BAQAUPe88yl0uUiPX3x8+R/7ObzhNyIHDCwOh7pdzmPn\nxxM82kn7aT4tRz1M+IU9yfh5SYnvFTX8/5HxczyuvXsqaWtO3+pzoqq7C9UqNnFfdXdBykDXHKRy\n+fjgExxM+EW9aH7vA+z69OOi4sBAnIICKCz0qF7ochHSoePx1QODcPJcVh3Ao96Jgtu0pe75Xdn3\nzb9LXN5r7Qb6Hcyk56q1NL39ztPeNJHaTEcOUqn67jmAb1AQAHsmf8m2Z/8GQHZiIr7BwdTpfC5H\nNv4OgE9QEKGdO+MbWrd4/eykROp2j/VoMyy2BwD+ERElvmfUsBEUulzsn/mtR7lr314SX3iezDWr\nwMeXqGHD6fjWu/gGB5P6z/e8s8EitYTCQSrV6gH98Q0OJiy2B9FPPkX7195k6+OPkDZ/HjnJSXR8\n6102PXAv+VlZtBk3Ht+wcJz8/OL1d0/8hA5vvkPTW29n/4zphMX2oOWx0Up/Ouo4JmrYcNIWzCc/\nPd2jPG3+j6TN//H46x9/wCcoiOjRY0j94H1wHO/vAJEzlE4rSaU6vG4th5YtJfX9d0kY8wTN77qH\n4NatcfLy2HD7rQQ0asRFq9dx6dZEgqNbs3fKZFz7jp+T3v2vz9n16ce0f+Nt+qTspsu/ppD0yv8B\ncHSffe46NKYLdTp2Yt83U8vUv/0zpuNfvwFBrVp5Z4NFagkdOUiVyVq3FoCgVtHkJCWRtWYVS7vG\nENK2HU5BPjlJSZz39TQyV604vlJhIVtHP0biS+MJbNqM3JRkQtp3ACBz5QrrPRoNG05BdjYHZn9X\ntk4dO1rQUYOIB4WDVJnwi3oBkJuS7FGevS0BgOBz2hDR7zLWxw231s3PyCA/IwOAZnfeQ8aypWQn\nbLXqRQ0bwYHvZ5d5qGqja/+K68Af5O7YUZ5NEan1FA5SKc6fNoO0hQs4smkTFBYQ3rMXLf7nIfZN\n+zc5SUkARI8ey5GELeQdPEho53OJfnIs+6f9m/SfFhS3E9bjAsJ7Xczh39bjVzeMqOEjqN//ClYP\nvMJ6z7AeFxDcKpqEp8aU2KeYf00mc/UqDm/YgPH1Jeq6YUQNG1E0Ka+GHTnUGzSYqDvuI+icNviE\nhODatZOD079h34T3cPLyCO15MR2mTC9x3UOLf2LbbXEANBg2kuhX37HqpDwzmgOTv/AoC+rQiWaj\nn6buBT3B+JC7PYEdzz5J9ob15W5LznynDAdjTBCwGAh01//GcZz/NcZ8BvQFDrmr3uY4zlpjjAHe\nBq4Cst3la9xt3Qo8467/ouM4n7vLY4HPgGBgNvCw49Swn1Ypl8w1q2lyw00EtWyFk59Pbkoyic8/\nx66JnxTX8a9fn/Yvv4p/gwbk7trJjnffLp7fcIyTn0/UdcNpPfZpKCwkY+kvrL7y8uIRTieKGjaC\nvIwMDs6bW2KfshMSaHLTLQQ1aw7GcGTLZjbecyd7v57i3Y33Ar96EWQtXcK+j98nPzOTOud3o+nD\nT+DfsCGp4/5G9u/r2XzdVR7rBDRtxjnvfUzmogVWe1tuuA4nN7f49dHUFI/lwZ3OpcPXM8n48XsS\nR90DQMh5XTHukWblaUtqB3Oq72D3l30dx3EOG2P8gSXAw8B9wHeO43zzp/pXAaMoCoeewNuO4/Q0\nxtQHVgE9AAdYDcQ6jpNujFkBPAQspygc3nEcZ84p+q7wqCb6M6EV+zOhpzsJrunjT9Hw5ttZ17V9\nicuj7n6AZk8+w2+XdCNvf9HF+mO/7f8a05rC7NL73WHabFypKSQ9cn+pdcra1qlUdBLcOGMqtP6Z\nblzFfm8u88475ZGD+zf4w+6X/u7HyXp3LfCFe71lxph6xpgmQD9gnuM4aQDGmHnAQGPMQiDMcZxl\n7vIvgKHAScPho8mzTtV1qSQZN/5PdXehWiVU8LN3eMgdp7VebNN2RAYFs7CU9W+66S5Styczr+fV\nxWXndu1ONBA/6BbyXK4S16sf1YjYbrFMXriCPSfpW1naKotVFdx/R+95rELrn+kq8t137w1Dyly3\nTNccjDG+FP2m3xZ433Gc5caY+4GXjDHPAfOBsY7jHAWaAaknrL7TXXay8p0llIuc9Ywx+Pr5EdWi\nKd36XMy6n5eXWC+iYSRRLZrxw1f2vaQA7nzuCYJDQsg4kMbqhUtY/8vxkV5NolsAEBQSzM1PPkRk\n40ZkpmewfN5CNixbVa62pPYoUzg4jlMAdDXG1AOmG2NigKeAvUAAMAEYA4yvrI7+WXkSULxrwf0j\nq7sL1ar/P1+p0Pqrnyn7LcK7bUrBJ7DovP/BaV9jnnyYfiWcVmgy6jEKXS4a/d9T9DuUUVwelpHI\nrr3bObJuDcbXl/qDh/KXkSPpmPAr+yd+BEDj5qEAXDNiCHsnvE/C+rVEDBrMlTffwTnzppG5cH6Z\n2yqL2LdfLnPdkoy78ZoKrX+mu/ej16vkfco1WslxnAxjzE/AQMdxXnMXHzXGTAKecL/eBbQ4YbXm\n7rJdFJ1aOrF8obu8eQn1K03nv9m3cT6bbPz7gOrugpTR5uGD8QkOps753Wgy6nFaPP8yqc+NtepF\nDB5K5pJFFJwQDACZ8QvJjF94/PWiBZjAIJo8+Aj7J00oGqXlPod/YOpk9k0oug364WU/E9SmPY3v\nf6g4HMrUltQap5whbYxp6D5iwBgTDPwF2Oy+jnDsgvVQYIN7lZnALabIRcAhx3H2AHOBAcaYCGNM\nBDAAmOtelmmMucjd1i3ADESEnN9/48iqFez/9CNSn3+aRjfdTkBLz9ncwR07E9yuA+mzSh7a+mfp\nc2bhF1GfgOYtAch3B0rWUs873GYtXUJw25IvfpfWltQeZTlyaAJ87r7u4ANMdRznO2PMAmNMQ4qu\nfq+laPQSFI02ugrYRtFQ1tsBHMdJM8a8ABy78f74YxengQc4PpR1Dqe4GC1yNsr+/TcAAlu0wrXj\n+PDRiCF/pTAnm4x5Zf2x8ZwVnuuehMifRwEZcEq5f1VpbUntUZbRSuuBbiWU9y+lvgM8WMqyicDE\nEspXATGn6ovI2Sw09gLAnldQf/BQMubPK/Pw0oiBQ8g7eADXrqLxIUfWrCQ/I526vS4lc/FPxfXC\nLu5NzuaN5WpLag/NkBapgdpOmkLWz4vJSdiCU1BAaI8LibrzftJmfetx1FCnayyBLVqS+uJzJbZz\nzj8/5ci6X8nZvBHj60vE1ddSf8hQdoz7W/Fv+05eHnvefYNmY56lICuTI+t/JWLgYEIv7MXW64eW\nqy2pPRQOIjVQ9vq1NBgWR0DzFjgF+bh2pLDr1Zf4Y/LnHvUihgwlP/MQmYvml9hObuJ2IkdcT0CT\npmAMOQlbSXrsQdK+9Zi7WnRB2cfQ6Ja7aPLQExxN2k7ig3dyeOXycrcltcMpZ0jXYKfdcY1Wqtho\nJc2Qrp4Z0rWFZkhXTI2ZIX22GhATxa2XtKJ1wxCC/X3ZnZHLzLW7mbg4mbwC+z9nzFUduPXSVkyK\nT+bVOfbdQgEahQUy+9FLCAn0o8e4+WS7CoqXRdYN4NEB7bi4bQPqBvmRcjCbSfHJfLdub7nbEhGp\nKIVDKeqF+LM8MY2J8clk5ebRpXk4D17ehsjQQF6atdmjbptGdRjWoxlZuXknbfOJQe3JdhUQEui5\n242B92/uRr0Qf17/fisHDrsYEBPFKyPPIze/kB9/31/mtkREvEF/Ca4UU1fs5J1525i/cT8rEtP5\ndHEyny9JYUjXJlbdp4d05F+/pJCZk19CS0VioyPo3S6SSfHJ1rLoyBC6NA/n5e+28N26vSzbnsb4\nGZvYuCuTQV0al6stERFvUDiUQ0Z2Hv6+nrtsQEwUrSPr8Mmi5FLX8zFFAfLPBdtJz7aPLvx8itrM\nyvUMl8zcfGvo+anaEhHxBoXDKfgYCPL3oXuretx0cUu+Xn58PHegnw9PDmrPG3MTyMkr/Zz/yJ4t\nCPDzYcqykseCJ+w7zLodGYy6og2tGoRQJ9CXod2b0r1VPb5evrNcbYmIeINOWJ/C6nGXE+jvC8C3\na3bz6vfHLzbf3a81f2QdZdbaPaWuHx7sz0NXtGXM1N/ILyx9lMG9n63hvZu7MefxSwHIyy/k6Wkb\nWJ6YVlynrG2JiFSUwuEUbvhoBcH+vnRpHs79/c/hmSGdeGHmJppFBHP7pdHc/ol9S+MTPTKgLetS\nM1i89UCpdYyBl0d0oV6IP49NWcfBwy76dIjkhevOJSM7jyUJB8vcloiINygcTmHT7iwA1qRkkJ7t\n4h8juvDZkmQeGdCO+K0HSDpwhLpBRbvRx0CAnw91g/zIys2nbaM6XBfbjJs/XllcJ8h9FBIa5EdB\nocPR/EL6dWjIZZ0aMuj1JaQcLBpDvzIpncbhQTw+sD1LEpaWuS0REW9QOJTDRndQNI8IpnVkCB2b\nhjEgxnNC0429WnJjr5Zc9o9FtGoQgr+fD1/d39Nqa+HYvnyzcifPTd9I64Z1yHYVFAfDMZt2Z3FZ\np0YAZW5LRMQbFA7l0L1VPQB2pufw7PSNhAT4eix/Pe48Vial89XyVNKOuFidksGtH6/0qHNp+0ju\n7tuaez9bQ2paURjszsghJMCX6MgQkg8cD4hzm4WxOz0HoMxtiYh4g8KhFB/d1p1l2w6ybf8RCgod\nureqx22XtmL2+j2kpuUAOdY6R/ML2Xsol5VJ6UDR0Ndjz49pFhEMwOrk9OJZzYu3HGB3eg7v3tSV\nDxYkkn7ERZ8ODRl0XmPGz9hUrrZERLxB4VCKDTsPMTS2Gc3qBZFf6LAzLYc3f0iwhpZ6Q7argDs+\nXcWjV7bjyavaExrox460HMZ9u5GpK7z/fiIip6Ib752FdOO9itGN9ypGN96rmKq68Z4mwYmIiEXh\nICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJR\nOIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhY\nFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIi\nFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiI\niEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4i\nImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWD\niIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXh\nICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJR\nOIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhY\nFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIi\nFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiI\niEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFuM4TnX3QURE\nahgdOYiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDFxhjJhpj9htjNpxQdr4xZqkx5jdj\nzCxjTJi7PMAYM8ldvs4Y089dHmKM+a8xZrMx5ndjzD+qaXOqnDf2n3vZ9+6y340xHxpjfKthc6qc\nt/bfCevOPLGt2s6Ln7+Fxpgtxpi17kejatgc73EcR48KPoA+QHdgwwllK4G+7ud3AC+4nz8ITHI/\nbwSspiikQ4DL3OUBQDwwqLq37UzZf+7XYe5/DTANiKvubTuT9p+77Dpg8olt1faHFz9/C4Ee1b09\n3nroyMELHMdZDKT9qbg9sNj9fB4wzP28M7DAvd5+IIOiD1S24zg/uctdwBqgeSV3vUbwxv5zv850\n1/GjKGDPihme3tp/xphQ4DHgxUruco3irf1X2ygcKs/vwLXu5yOAFu7n64BrjDF+xpjWQOwJywAw\nxtQDhgDzq6ivNdFp7T9jzFxgP5AFfFN13a1xTmf/vQC8DmRXZUdrqNP9+Z3kPqX0rDHGVF13vU/h\nUHnuAB4wxqwG6gIud/lEYCewCngL+AUoOLaSMcYPmAK84zhOYpX2uGY5rf3nOM6VQBMgEOhflR2u\nYcq1/4wxXYE2juNMr47O1kCn8/m70XGcLkBv9+PmKu2xl/lVdwdqK8dxNgMDAIwx7YGr3eX5wKPH\n6hljfgG2nrDqBCDBcZy3qq63NU8F9h+O4+QaY2ZQ9JvfvKrqc01yGvuvL9DDGJNM0fdCI2PMQsdx\n+lVtz2uG0/n8OY6zy/1vljFmMnAh8EXV9tx7dORQSY6NVDDG+ADPAB+6X4cYY+q4n/8FyHccZ6P7\n9YtAOPBItXS6Binv/jPGhBpjmrjL/Sj6Yd5cLZ2vAcq7/xzH+cBxnKaO40QDlwJbz9ZggNP6/PkZ\nYyLd5f7AYOCMHvGlIwcvMMZMAfoBkcaYncD/AqHGmAfdVf4DTHI/bwTMNcYUArtwH3oaY5oDT1P0\nhbbGfbryPcdxPqmq7agu3th/QB1gpjEmkKJfen7C/QNd23lp/521vLT/At3l/oAv8CPwcdVsQeXQ\nLbtFRMSi00oiImJROIiIiEXhICIiFoWDiIhYFA4iImJROIiIiEXhICIiFoWDiIhY/j9p+pz5M6Tj\noQAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<matplotlib.figure.Figure at 0x7f8aa5d3c240>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# plot blue/red when the values of bars under/above value of interest\n",
    "colors = list()\n",
    "# max_below_voi = max(avgs[avgs < value_of_interest])\n",
    "# max_above_voi = max(avgs[avgs > value_of_interest])\n",
    "\n",
    "# adjust bar color by y of interest\n",
    "for avg in avgs:\n",
    "    if avg == value_of_interest:\n",
    "        colors.append('lightslategrey')\n",
    "    elif avg > value_of_interest:\n",
    "        colors.append(cm.OrRd(avg / float(max(avgs))))\n",
    "    else:\n",
    "        colors.append(cm.Blues(avg / float(max(avgs))))\n",
    "\n",
    "bars = plt.bar(np.arange(len(avgs)), avgs, align='center', linewidth=0, color=colors)\n",
    "\n",
    "\n",
    "# add horizonal line of value of interest\n",
    "plt.axhline(y=value_of_interest, color='lightslategrey', linestyle='-')\n",
    "# plt.yticks(list(plt.yticks()[0]) + [value_of_interest])\n",
    "\n",
    "# date as xticks\n",
    "plt.xticks(np.arange(len(avgs)), list(avgs.index))\n",
    "\n",
    "# show only value of interest as y ticks\n",
    "plt.yticks([value_of_interest])\n",
    "\n",
    "\n",
    "# ticks/label setting\n",
    "plt.tick_params(\n",
    "    axis='both',          # changes apply to the x-axis\n",
    "    which='both',      # both major and minor ticks are affected\n",
    "    bottom='off',      # ticks along the bottom edge are off\n",
    "    top='off',         # ticks along the top edge are off\n",
    "    left='off',\n",
    "    right='off',\n",
    "    labelleft='on',\n",
    "    labelbottom='on')\n",
    "\n",
    "# add lable on bars\n",
    "for bar in bars:\n",
    "    h = bar.get_height()\n",
    "    plt.gca().text(bar.get_x() + bar.get_width() / 2., h - 5000, '{:d}'.format(int(h)), fontsize=15,\n",
    "        ha='center', va='bottom', color='white')\n",
    "\n",
    "# remove frame\n",
    "for spine in plt.gca().spines.values():\n",
    "    spine.set_visible(False)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
